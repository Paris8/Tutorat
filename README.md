# Dépôts auxquels j'ai contribué pendant mon tutorat
- [Template LaTeX pour faire des trombinoscopes](https://code.up8.edu/tuteurices/trombi)
- [Site web des tuteurices](https://code.up8.edu/tuteurices/tuteurices.up8.site)
